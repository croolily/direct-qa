package com.iconfitness.directqa.test.tests.local;

// LOCAL TEST
import com.iconfitness.directqa.test.tests.templates.local.BaseTest;

import com.iconfitness.directqa.pages.proform.PFHomePage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@ActiveProfiles("qa")
public class FirstTest extends BaseTest {

    @BeforeMethod
    public void loadHomePage() {
        driver.get().get("qa.proform.com");
    }

    @Test(groups = "proof")
    public void First() {
        
        PFHomePage pfHomePage = new PFHomePage(driver.get());
        pfHomePage.withFluent().clickTreadNavBarLink();
        Assert.assertEquals(driver.get().getTitle(), "Treadmills on Sale | In-Home & On-Demand Trainers | ProForm");    
        
    }

    @AfterTest
    public void killDriver() {
        driver.get().quit();
    }

}