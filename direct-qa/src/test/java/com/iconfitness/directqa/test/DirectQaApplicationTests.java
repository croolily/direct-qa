package com.iconfitness.directqa.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.iconfitness.directqa.DirectQaApplication;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.iconfitness.directqa.DirectQaApplication;

@SpringBootTest (classes = DirectQaApplication.class)
class DirectQaApplicationTests {

	@Test
	void contextLoads() {
	}

}
