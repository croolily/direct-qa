package com.iconfitness.directqa.util.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DriverFactory {

    private static WebDriver driver;

    @Bean
    public static WebDriver getDriver(String browser) {
        if (browser.equals("firefox"))
            driver = new FirefoxDriver(OptionsManager.getFirefoxOptions());
        else if (browser.equals("chrome"))
            driver = new ChromeDriver(OptionsManager.getChromeOptions());
        else if (browser.equals("ie"))
            driver = new InternetExplorerDriver(OptionsManager.getIEOptions());
        return driver;
    }
}