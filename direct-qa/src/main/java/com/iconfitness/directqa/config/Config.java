package com.iconfitness.directqa.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.iconfitness.directqa.objects.AddRun;
import com.iconfitness.directqa.service.SlackService;
import com.iconfitness.directqa.service.TestRailService;

@Configuration
public class Config {

	@Autowired
	private TestRailService testRailService;

	@Autowired
	private SlackService slackService;
	
	@Value("${direct-qa.testrail.projectid}")
	private String projectId;
	
	@Bean 
	public Integer qaTestRunId() {
		Integer testRunId = 3264;
		try {
//			AddRun addRun = testRailService.startTestRun(projectId);
//			slackService.postTestToSlack(addRun);
//			testRunId = addRun.getId();
//			System.out.println("from config test run Id = " + addRun.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return testRunId;
	}
}


