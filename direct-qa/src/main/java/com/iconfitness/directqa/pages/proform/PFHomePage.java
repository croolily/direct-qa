package com.iconfitness.directqa.pages.proform;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.testng.Assert;

@Component
@Lazy
public class PFHomePage extends LoadableComponent<PFHomePage> {

  // Autowired with Value
  @Autowired
  @Value("${proform.qa.domestic}")
  private String url;

  @Autowired
  private WebDriver driver;
  
  @Autowired
  private PFHPFluentInterface pfhpfi;

  
  public class PFHPFluentInterface {

    private PFHomePage pfhp;

    public PFHPFluentInterface(PFHomePage pfHomePage) {
      pfhp = pfHomePage;
    }

    public PFHomePage goToHomePage() {
      pfhp.driver.get(url);

      return new PFHomePage(pfhp.driver);
    }

    public PFHPFluentInterface clickSearchButton() {
      pfhp.searchButton.click();
      return this;
    }
    
    
    public PFHPFluentInterface setSearchString(String sstr) {
      clearAndType(pfhp.searchField, sstr);
      return this;
    }
    
    public void clickPFPlusNavBarLink(){
      pfhp.plusNav.click();
    }

    public void clickTreadNavBarLink() {
      pfhp.treadNav.click();
    }
  }
  
  // Header Nav Elements
  @Autowired
  @FindBy(id = "first-nav")
  private WebElement headerNav; // Full element.
  // Individual links.
  @Autowired
  @FindBy(linkText = "/free-equipment")
  private WebElement plusNav;
  @Autowired
  @FindBy(linkText = "/treadmills")
  private WebElement treadNav;
  @Autowired
  @FindBy(id = "search-input")
  private WebElement searchField;
  @Autowired
  @FindBy(id = "mo-search-button")
  private WebElement searchButton;

  public PFHomePage(WebDriver driver) {
    pfhpfi = new PFHPFluentInterface( this );
    this.driver = driver;
    //this.get(); // If load() fails, calls isLoaded() until page is finished loading
    PageFactory.initElements(driver, this); // Initialize WebElements on page
  }

  public PFHPFluentInterface withFluent() {
    return pfhpfi;
  }
  
  
  
  private void clearAndType(WebElement searchField, String sstr) {
    searchField.clear();
    searchField.sendKeys(sstr);
  }
  
  public void setSearchString( String sstr ) {
    clearAndType( searchField, sstr );
  }
  
  @Override
  protected void isLoaded() throws Error {
    Assert.assertTrue( isSearchFieldVisible(), "Search..." );
  }
  
  
  private boolean isSearchFieldVisible() {
    boolean result = false;
    if (this.searchField.getAttribute("placeholder").length() > 0){
      result = true;
    }
    return result;
  }

  @Override
  protected void load() {
    if ( isSearchFieldVisible() ) {
      Wait<WebDriver> wait = new WebDriverWait( driver, 3 );
      wait.until( ExpectedConditions.visibilityOfElementLocated( By.id("search-input") ) ).click();
    }
  }

}