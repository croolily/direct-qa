package com.iconfitness.directqa.objects;

import java.time.LocalDate;

public class GLOBALS {

	public static String firstName = "TEST";
	public static String testLastName = "test";
	public static String cardLastName = "card";
	public static String phoneNumber = "4357862344";
	public static String frenchPhoneNumber = "0374660240";
	public static String emailAddress = "janna.wolfley@iconfitness.com";
	public static Card[] CARD = new Card[4];
	public static Card[] STRIPE_CARD = new Card[4];
	static {
		LocalDate date = LocalDate.now();

		CARD[0] = new Card("4444", "3333", "2222", "1111", date.getMonthValue(), date.getYear(), "999", "vi");
		CARD[1] = new Card("4012", "0000", "3333", "0026", date.getMonthValue(), date.getYear(), "999", "vi");
		CARD[2] = new Card("5424", "1802", "7979", "1732", date.getMonthValue(), date.getYear(), "999", "mc");
		CARD[3] = new Card("6011", "0009", "9130", "0009", date.getMonthValue(), date.getYear(), "999", "di");
	}

	static {
		LocalDate date = LocalDate.now();

		Integer month = date.getMonthValue();
		// month needs to be two characters. If it's 1, it'll expect a character after it, so this sets it to 10
		if (month == 1) month = 10;

		Integer year = date.getYear();
		//converts year to a string and pulls the last two digits
		String stringYear = year.toString().substring(2);
		//converts back to an integer and assigns the two-digit value to year
		year = Integer.parseInt(stringYear);

		STRIPE_CARD[0] = new Card("4242", "4242", "4242", "4242", month, year, "222", "vi");
		STRIPE_CARD[1] = new Card("5555", "5555", "5555", "4444", month, year, "222", "mc");
		STRIPE_CARD[2] = new Card("6011", "1111", "1111", "1117", month, year, "222", "di");
//		STRIPE_CARD[3] = new Card("3782", "8224", "6310", "005", date.getMonthValue(), year, "222", "ax");
	}

	
	// Addresses
	public static String US_address = "1500 South 1000 West";
	public static String US_postalCode = "84321";
	public static String US_city = "Logan";
	
	public static String US_separateAddress = "1285 North State";
	public static String US_separatePostalCode = "83263";
	public static String US_separateCity = "Preston";

	public static String CA_address = "1405 Isabella Street";
	public static String CA_postalCode = "P7E 5B8";
	public static String CA_city = "Thunder Bay";
	public static String CA_province = "Ontario";
	
	public static String UK_address = "big ben";
	public static String UK_postalCode = "W1F 8JB";
	public static String UK_city = "London";

	public static String FR_address = "152 La Roux";
	public static String FR_postalCode = "75000";
	public static String FR_city = "Paris";

	public static String Validate_US_Address = "123 donot ship";
	public static String Validate_CA_address = "123 donot ship";
	
	public static String AlaskaAddress = "1100 Garden Way";
	public static String AlaskaPostal = "99701";
	public static String AlaskaCity = "Fairbanks";

	// financing info
	public static String TDfirstName = "Donna";
	public static String TDlastName = "Wood";
	public static String TDaddress = "630 Baskins Road";
	public static String TDpostalCode = "38015";
	public static String TDcity = "Burlison";
	public static String TDbirth = "09301965";
	public static String TDsocial = "666727464";
	public static String TDrefId = "3210820282320697";

	// fortiva mimic info
	public static String FORTIVAfirstName = "Mimic";
	public static String FORTIVAlastName = "Approve";
	public static String FORTIVAaddress = "123 Test Dr";
	public static String FORTIVApostalCode = "73344";
	public static String FORTIVAcity = "Austin";

	// paypal credentials
	public static String paypalUserName = "tcox_1345667054_per@iconfitness.com";
	public static String paypalPassword = "iconbuyer1";

	// CSR credentials
	public static String adminUserName = "janna";
	public static String adminPassword = "myadminaccount";
}
