package com.iconfitness.directqa.objects;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddRun {

	private Integer id = null;
	@JsonProperty("suite_id")
	private Integer suiteId = null;
	private String name = null;
	private String description = null;
	@JsonProperty("milestone_id")
	private Integer milestoneId = null;
	@JsonProperty("assignedto_id")
	private Integer assignedToId = null;
	@JsonProperty("include_all")
	private Boolean includeAll = null;
	@JsonProperty("is_completed")
	private Boolean isCompleted = null;
	@JsonProperty("completed_on")
	private Date completedOn = null;
	private Object config = null;
	@JsonProperty("config_ids")
	private Object[] configIds;
	private String url = null;
	@JsonProperty("project_id")
	private Integer projectId = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(Integer milestoneId) {
		this.milestoneId = milestoneId;
	}

	public Integer getAssignedToId() {
		return assignedToId;
	}

	public void setAssignedToId(Integer assignedToId) {
		this.assignedToId = assignedToId;
	}

	public Boolean getIncludeAll() {
		return includeAll;
	}

	public void setIncludeAll(Boolean includeAll) {
		this.includeAll = includeAll;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public Date getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	public Object getConfig() {
		return config;
	}

	public void setConfig(Object config) {
		this.config = config;
	}

	public Object[] getConfigIds() {
		return configIds;
	}

	public void setConfigIds(Object[] configIds) {
		this.configIds = configIds;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

}
