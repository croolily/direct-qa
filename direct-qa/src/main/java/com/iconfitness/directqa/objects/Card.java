package com.iconfitness.directqa.objects;

public class Card {

	private String ccFirst4 = null;
	private String ccSecond4 = null;
	private String ccThird4 = null;
	private String ccLast4 = null;
	private int expMonth = 0;
	private int expYear = 0;
	private String cvv = null;
	private String type = null;

	public Card(String ccFirst4, String ccSecond4, String ccThird4, String ccLast4, int expMonth, int expYear, String testCardCvv, String type) {
		this.ccFirst4 = ccFirst4;
		this.ccSecond4 = ccSecond4;
		this.ccThird4 = ccThird4;
		this.ccLast4 = ccLast4;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.cvv = testCardCvv;
		this.type = type;
	}

	public String getCcFirst4() {
		return ccFirst4;
	}

	public void setCcFirst4(String ccFirst4) {
		this.ccFirst4 = ccFirst4;
	}

	public String getCcSecond4() {
		return ccSecond4;
	}

	public void setCcSecond4(String ccSecond4) {
		this.ccSecond4 = ccSecond4;
	}

	public String getCcThird4() {
		return ccThird4;
	}

	public void setCcThird4(String ccThird4) {
		this.ccThird4 = ccThird4;
	}

	public String getCcLast4() {
		return ccLast4;
	}

	public void setCcLast4(String ccLast4) {
		this.ccLast4 = ccLast4;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
