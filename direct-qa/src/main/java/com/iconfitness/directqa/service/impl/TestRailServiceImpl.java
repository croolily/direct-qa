package com.iconfitness.directqa.service.impl;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iconfitness.directqa.objects.AddRun;
import com.iconfitness.directqa.service.TestRailService;

@Service("testRailService")
public class TestRailServiceImpl implements TestRailService {
	
	@Override
	public AddRun startTestRun(String projectId) {
		Date date = new Date();
		String sendPostURL = "https://iconweb.testrail.net/index.php?/api/v2/add_run/" + projectId;
		Map<String, Object> addRunData = new HashMap<String, Object>();
		addRunData.put("suite_id", "1");
		addRunData.put("name", date.toString());
		addRunData.put("include_all", true);
		AddRun results = ((AddRun) post(sendPostURL, addRunData, AddRun.class));
		System.out.println("From the service " + results.getId());
		return results;
	}
	

	@Override
	public Object get(String url, Map<String, Object> runData, Class<?> clazz) {
		new HttpEntity<Map<String, Object>>(runData, createHeaders("janna.wolfley@iconfitness.com", "jwICON!2014"));
		RestTemplate template = new RestTemplate();
		ResponseEntity<?> response = template.getForEntity(url, clazz, runData);
		return response.getBody();
	}

	private Object post(String url, Map<String, Object> runData, Class<?> clazz) {
		HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(runData, createHeaders("janna.wolfley@iconfitness.com", "jwICON!2014"));
		RestTemplate template = new RestTemplate();
		ResponseEntity<?> response = template.postForEntity(url, request, clazz);
		return response.getBody();
	}

	@Override
	// provide the headers for the post
	public HttpHeaders createHeaders(final String username, final String password) {
		HttpHeaders headers = new HttpHeaders();
		String auth = username + ":" + password;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		headers.add("Authorization", "Basic " + new String(encodedAuth));
		return headers;
	}

	/*
	 * Method to mark the status of a test //posting results for a test case status_id 1 = passed; status_id 2 = blocked; status_id 3 = re-test on un-tested test cases; status_id 4
	 * = re-test on already tested cases status_id 5 = failed
	 */
	@Override
	public void setTestStatus(boolean itWorks, Integer testRunId, Integer caseId, String testComment, Class<?> clazz, boolean markRetest) {
		String sendPostURL = "https://iconweb.testrail.net/index.php?/api/v2/add_result_for_case/" + testRunId + "/" + caseId;
		Map<String, Object> testStatus = new TreeMap<String, Object>();
		testStatus.put("comment", testComment);
		if (itWorks && !markRetest) {
			testStatus.put("status_id", "1");
		} else if (itWorks && markRetest) {
			testStatus.put("status_id", "4");
		} else if (!itWorks && !markRetest) {
			testStatus.put("status_id", "5");
		}
		post(sendPostURL, testStatus, clazz);
	}
	
}
