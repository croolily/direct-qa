package com.iconfitness.directqa.service;

import java.util.Map;

import org.springframework.http.HttpHeaders;

import com.iconfitness.directqa.objects.AddRun;

public interface TestRailService {

	AddRun startTestRun(String projectId);

	Object get(String url, Map<String, Object> runData, Class<?> clazz);

	HttpHeaders createHeaders(String username, String password);

	void setTestStatus(boolean itWorks, Integer testRunId, Integer caseId, String testComment, Class<?> clazz, boolean markRetest);
	
}
