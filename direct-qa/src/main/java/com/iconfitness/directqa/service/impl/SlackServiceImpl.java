package com.iconfitness.directqa.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iconfitness.directqa.objects.AddRun;
import com.iconfitness.directqa.service.SlackService;

@Service("slackService")
public class SlackServiceImpl implements SlackService {

	@Value("${direct-qa.slack.channel}")
	private String slackChannel;
	@Value("${direct-qa.slack.service.url}")
	private String slackService;
	
	@Override
	public void postTestToSlack(AddRun addRun) throws JsonProcessingException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		Map<String, String> payload = new HashMap<String, String>();
		payload.put("channel", slackChannel);
		payload.put("text", getProject(addRun.getProjectId()) + " <" + addRun.getUrl() + ">");
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(payload));
		parameters.add("payload", mapper.writeValueAsString(payload));

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForEntity(slackService, request, String.class);
	}

	private static String getProject(Integer projectId) {
		switch (projectId) {
		case 15:
			return "It works QA";
		case 14:
			return "It Works Production";
		}
		return "Some project I don't know about";
	}

}
