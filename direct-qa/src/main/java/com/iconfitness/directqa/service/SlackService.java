package com.iconfitness.directqa.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.iconfitness.directqa.objects.AddRun;

public interface SlackService {

	void postTestToSlack(AddRun addRun) throws JsonProcessingException;

}
